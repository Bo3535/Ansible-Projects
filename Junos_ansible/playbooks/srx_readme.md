Documentation for playbook
===========================

# Playbook details

* This playbook will use the PyEZ module to connect to the SRX
through Netconf ssh on port 830.

* It will fetch the System Version number and store it in a
.json file in the playbooks directory

* It will then post it in the ansible output