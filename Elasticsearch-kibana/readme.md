This is a guide how to install Elasticsearch and Kibana on a machine.
======================================================================

* This can only be done on a linux machine, keep note of that. 

* I have only tested on Debian and unbuntu. I do not guarantee. 

__This example uses sudo__

__This requires 2.3.2__

__This requires about 7 gb of ram__

* Make sure to use ssh-keys between the machines.
* You want to change values in the, inventory, and in group_vars


* 1. install the two following roles thorugh ansible galaxy.
=============================================================

---

**require root**

$ ansible-galaxy install elastic.elasticsearch

$ ansible-galaxy install jradtilbrook.kibana


---

* 2. Use the playbooks stored in /ansible/playbooks. But change hosts
=====================================================================

---

cd /etc/ansible



__ansible-playbook -i inventory/inventory playbooks/elastic -K__

This will promt you with sudo passwd

After this is finished use the command

__ansible-playbook -i inventory/inventory playbooks/kibana -K__

---

* If all this went without errors all has been installed correctly.


* 3. Connect to the kibana using a webrowser with the url: http://localhost:5601
================================================================================

